#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#include "terminal.h"

struct editorConfig {
	struct termios orig_termios;
};

struct editorConfig E;

void die(const char* err) {
	editorRefreshScreen();
	perror(err);
	exit(1);
}

void disableRawMode(){
	// Restore orig_termios
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &E.orig_termios) == -1)
		die("tcsetattr");
}

void enableRawMode() {
	tcgetattr(STDIN_FILENO, &E.orig_termios);
	atexit(disableRawMode); //Disable raw on exit

	struct termios raw = E.orig_termios;
	// Set certain flags to false
	raw.c_iflag &= ~(
			BRKINT |
			ICRNL |
			INPCK |
			ISTRIP |
			IXON
	);
	raw.c_oflag &= ~(
			OPOST
	);
	raw.c_cflag |= (CS8);
	raw.c_lflag &= ~(
			ECHO | 
			ICANON |
			IEXTEN |
			ISIG
	);
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 1;
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr");
}

char editorReadKey() {
	int nread;
	char c;
	while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
		if (nread == -1 && errno != EAGAIN) die("read");
	}
	return c;
}

void editorRefreshScreen() {
	write(STDOUT_FILENO, "\x1b[2J", 4);
	write(STDOUT_FILENO, "\x1b[H", 3);
}
