CFLAGS=-c -Wall

all: link

link: main terminal
	$(CC) main.o terminal.o -o modedi

main: main.c
	$(CC) $(CFLAGS) main.c -o main.o

terminal: terminal.c
	$(CC) $(CFLAGS) terminal.c -o terminal.o
