#ifndef _TERMINAL_H_INCLUDED
#define _TERMINAL_H_INCLUDED

void die();
void enableRawMode();
void editorRefreshScreen();
char editorReadKey();

#endif //_TERMINAL_H_INCLUDED
