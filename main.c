#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "terminal.h"

#define CTRL_KEY(k) ((k) & 0x1f)

void editorProcessKeypress() {
  char c = editorReadKey();
  switch (c) {
    case CTRL_KEY('q'):
      exit(0);
      break;
  }
}

int main() {
	enableRawMode();

	// Make a main loop: read from STDIN until eof.
	while (1) {
		editorProcessKeypress();
	}
	return 0;
}
